var colors = [];

var colorsqueue = [];

var strips = [];

var font;

//var remotepath = "http://escenaconsejo.org/p5/Typographic-Expressive-Words/kerning-freak/";
var remotepath = "";

function preload(){
	font = loadFont(remotepath+"Arvo-Regular.ttf");
}

function setup() {
  createCanvas(windowWidth,windowHeight);
  noStroke();
  textFont(font);
//  stroke(255);

  colors.push(new col("#171521","My helmet"));
  colors.push(new col("#FF9998","Williamsburg Bridge"));
  colors.push(new col("#FF0072","Our vest"));

  colors.push(new col("#6EA6CD","Clear sky"));
  colors.push(new col("#6D6D6D","Street"));
  colors.push(new col("#8BA1AF","Manhattan Bridge"));
  colors.push(new col("#CAA551","Road lines"));

  colors.push(new col("#769673","East River"));
  colors.push(new col("#0471C0","My Bike"));
  colors.push(new col("#01FB55","Our Locks"));
  colors.push(new col("#EE004D","Our Locks"));

  colors.push(new col("#1C1C1C","Our Locks"));
  colors.push(new col("#D4C4BC","My Handlebar"));
  colors.push(new col("#C0C0BE","Some parts of my bike"));
  colors.push(new col("#D2002E","Melissa's Bike"));

  colors.push(new col("#FFFFFF","Lights"));
  colors.push(new col("#FDC551","Traffic Lights"));
  colors.push(new col("#048C68","Green light"));
  colors.push(new col("#FD3434","Red light"));

  colors.push(new col("#E7340B","Pedestrian stop"));
  colors.push(new col("#024E4A","Street signs"));
  colors.push(new col("#F9CA32","Yellow light"));
  colors.push(new col("#00D28C","My glasses"));


  var st;
  var x=0;
  var w=0;
  var i=0;
  var c;
  for(var i=0; w<width/2; i++){
	w = pow(1.16,i);
	c = random(colors);
	colorsqueue.push(c);
	st = new strip(x,w,0.01,c);
	x += w;
	strips.push(st);
  }

  setColors();

}

// Assign the colors to the strips from the queue
function setColors(){
	var N = strips.length;
	for(var i=0; i<N; i++){
		strips[i].setColor(colorsqueue[N-i-1]);
	}
}

function moveColors(){
	var lastcolor = colorsqueue[colorsqueue.length-1];
	var newcolor;
	do{
		newcolor = random(colors);
	}while(newcolor.color==lastcolor.color);
	colorsqueue.push(newcolor); // Add a new color
	colorsqueue.shift(); // Remove the first one
	setColors();

}

function draw() {

	var reset;
	var txt = "";
	for(var i=0; i<strips.length; i++){
		strips[i].draw();
		reset=strips[i].grow();

		if(strips[i].mouseInside()){
			txt = strips[i].col.text;
		}
	}



	fill(255);
	strokeWeight(3);
	stroke(10);
	textSize(42);
	text(txt,mouseX,mouseY);

  	if(reset){
		moveColors();
		for(var i=0; i<strips.length; i++){
			strips[i].reset();
		}
	}
}




function col(c,t){
	this.color = c;
	this.text = t;
}

function strip(x,w,s,c){
	this.xi = x;
	this.wi = w;
	this.col = c;

	this.t = 0;
	this.w = this.wi;
	this.winc = s*this.wi;

	// Returns true when has exceeded double 
	this.grow = function(){ 
		this.t += this.winc;
		this.w += this.winc;

		return this.w>=2*this.wi;
	}

	this.setColor = function(c){
		this.col = c;
	}

	this.setSpeed = function(s){

	}

	this.reset = function(){
		this.t = 0;
		this.w = this.wi;
	}

	this.mouseInside = function(){
		var x = this.xi + this.t;
		return mouseX>=x && mouseX<=x+this.w;
	}

	this.draw = function(){
		push();
		translate(this.xi+this.t,0);
		fill(this.col.color);
		rect(0,0,this.w,height);
		pop();
	}

}





